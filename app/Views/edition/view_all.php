<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container main">
    <h2>Все книги</h2>

    <?php if (!empty($edition) && is_array($edition)) : ?>

        <?php foreach ($edition as $item): ?>

            <div class="card mb-3" style="max-width: 540px;">
                <div class="row">
                    <div class="col-md-4 d-flex align-items-center">


                        <?php if ($item['name'] == 0) : ?>
                            <img height="100" width="100" src="https://im0-tub-ru.yandex.net/i?id=c87ce01317fcaa037b11e5a40c97528d&n=13" class="card-img">
                        <?php else:?>
                            <img height="225" src="<?= esc($item['picture_url']); ?>" class="card-img" alt="<?= esc($item['name']); ?>">
                        <?php endif ?>
           


                            
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= esc($item['name']); ?></h5>
                            <a href="<?= base_url()?>/index.php/edition/view/<?= esc($item['id']); ?>" class="btn btn-primary">Просмотреть</a>
<a href="<?= base_url()?>/index.php/edition/edit_edition/<?php echo $item['id']?>" class="btn btn-primary">Редактировать</a>
                        </div>
                    </div>
                </div>
            </div>

        <?php endforeach; ?>

    <?php else : ?>

        <p>Невозможно найти экземпляры.</p>

    <?php endif ?>
</div>
<?= $this->endSection() ?>
