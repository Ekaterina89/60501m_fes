<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php echo '<main role="main" class="container">'; ?>	
	<?php if (!empty($delivery)) :
	?>	
   <table class="table">
        <thead class="text-white bg-primary">
        <tr>
<th scope="col">№ экземпляра</th>            
<th scope="col">Название книги</th>
			  <th scope="col">ФИО читателя</th>
            <th class="text-center" scope="col">Дата получения книги</th>
            <th scope="col">Плановая дата возврата</th>
            <th scope="col">Фактическая дата возврата</th>
			<th scope="col">   </th>
        </tr>
        </thead>
        <tbody>
		<?php foreach ($delivery as $delivery_item): ?>
		<tr>
<th><?php echo $delivery_item['ID_copy']?></th>            
<th><?php echo $delivery_item['name']?></th>
            <td><?php echo $delivery_item['fullname']?></td>
            <td class="text-center"><?php echo $delivery_item['take_date']?></td>
            <td><?php echo $delivery_item['return_date_plan']?></td>
            <td><?php echo $delivery_item['return_date_fact']?></td>
			</form>
        </tr>
<td><a href="<?= base_url()?>/index.php/edition/edit/<?php echo $delivery_item['id']?>" class="btn btn-primary">Редактировать</a></td>
            <td><a href="<?= base_url()?>/index.php/edition/delete/<?php echo $delivery_item['id']?>" class="btn btn-danger">Удалить</a></td>
		<?php endforeach; ?>
        </tbody>
    </table>
 <?php else : ?>
        <p> Экземпляр в наличии.</p>
    <?php endif ?>
<?= $this->endSection() ?>
