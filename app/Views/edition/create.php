<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">

    <?= form_open_multipart('edition/store'); ?>
    <div class="form-group">
       <label for="ID_copy">Название книги</label>
        <select  id="ID_copy" class="form-control <?= ($validation->hasError('ID_copy')) ? 'is-invalid' : ''; ?>" name="ID_copy"  value="<?= old('ID_copy'); ?>">
            <?php
		    foreach ($copy as $copy_item):
            {
                echo "<option value=".$copy_item['id'].">".$copy_item['name'].", №". $copy_item['id']."</option>";
            }
			endforeach;
			?>
    </select>
        <div class="invalid-feedback">
            <?= $validation->getError('ID_copy') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="ID_reader">ФИО читателя</label>
        <select id="ID_reader" class="form-control <?= ($validation->hasError('ID_reader')) ? 'is-invalid' : ''; ?>" name="ID_reader" value="<?= old('ID_reader'); ?>" >  
		<?php
          foreach ($reader as $reader_item):
            {
                echo "<option value=".$reader_item['ID'].">".$reader_item['fullname']."</option>";
            }
			endforeach;
	    ?>	
        </select>
        <div class="invalid-feedback">
            <?= $validation->getError('ID_reader') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="take_date">Дата получения книги</label>
       <input type="date" class="form-control <?= ($validation->hasError('take_date')) ? 'is-invalid' : ''; ?>" name="take_date"
               value="<?= old('take_date'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('take_date') ?>
        </div>
    </div>
        <div class="form-group">
            <label for="return_date_plan">Плановая дата возврата</label>
            <input type="date" class="form-control <?= ($validation->hasError('return_date_plan')) ? 'is-invalid' : ''; ?>" name="return_date_plan" value="<?= old('return_date_plan'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('return_date_plan') ?>
            </div>
        </div>

         <div class="form-group">
            <label for="return_date_fact">Фактическая дата возврата</label>
            <input type="date" class="form-control <?= ($validation->hasError('return_date_fact')) ? 'is-invalid' : ''; ?>" name="return_date_fact" value="<?= old('return_date_fact'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('return_date_fact') ?>
            </div>
        </div>
    <div class="form-group">
    <button type="submit" class="btn btn-primary" name="submit">Создать</button>
    </div>
    </form>


    </div>
<?= $this->endSection() ?>

