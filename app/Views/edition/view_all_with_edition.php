<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
<?php if (!empty($delivery) && is_array($delivery)) : ?>
    <h2>Все книги:</h2>
    <div class="d-flex justify-content-between mb-2">
    <?= $pager->links('group1','my_page') ?>
<?= form_open('edition/viewAllWithEdition', ['style' => 'display: flex']); ?>
        <select name="per_page" class="ml-3" aria-label="per_page">
            <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
            <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
            <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
            <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
        </select>
        <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
        </form>
 <?= form_open('edition/viewAllWithEdition',['style' => 'display: flex']); ?>
        <input type="text" class="form-control ml-3" name="search" placeholder="Имя или описание" aria-label="Search"
               value="<?= $search; ?>">
        <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
        </form>
    </div>
<table class="table table-striped">
        <thead>
            <th scope="col">№ экземпляра</th>
            <th scope="col">Обложка</th>
            <th scope="col">Коэффициент изношенности(макс=1)</th>
            <th scope="col">Название книги</th>
            <th scope="col">ФИО читателя</th>
            <th scope="col">Дата получения книги</th>
            <th scope="col">Плановая дата возврата</th>
            <th scope="col">Фактическая дата возврата</th>
            <th scope="col">Управление</th>

        </thead>
        <tbody>
    <?php foreach ($delivery as $delivery_item): ?>
        <tr>
        <td><?php echo $delivery_item['ID_copy']?></td>            
        <td>
        <img height="90" src="<?= esc($delivery_item['picture_url']); ?>" class="card-img" alt="<?= esc($delivery_item['name']); ?>">
        </td>
        <td><?= esc($delivery_item['wear_factor']); ?></td>
        <td><?= esc($delivery_item['name']); ?></td>
        <td><?= esc($delivery_item['fullname']); ?></td>
        <td class="text-center"><?php echo $delivery_item['take_date']?></td>
        <td><?php echo $delivery_item['return_date_plan']?></td>
        <td><?php echo $delivery_item['return_date_fact']?></td>
        <td>
                <a href="<?= base_url()?>/edition/view/<?= esc($delivery_item['id']); ?>" class="btn btn-primary btn-sm">Просмотреть</a>
                <a href="<?= base_url()?>/edition/edit/<?= esc($delivery_item['id']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                <a href="<?= base_url()?>/edition/delete/<?= esc($delivery_item['id']); ?>" class="btn btn-danger btn-sm">Удалить</a>
        </td>
        </tr>
    <?php endforeach; ?>
        </tbody>
        </table>

<?php else : ?>
    <div class="text-center">
    <p>Книги не найдены </p>
        <a class="btn btn-primary btn-lg" href="<?= base_url()?>/edition/create"><span class="fas fa-tachometer-alt" style="color:white"></span>&nbsp;&nbsp;Выдать книгу</a>
    </div>
<?php endif ?>
</div>
<?= $this->endSection() ?>
