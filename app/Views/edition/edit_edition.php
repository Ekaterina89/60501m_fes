<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container" style="max-width: 540px;">

    <?= form_open_multipart('edition/update_edition'); ?>
    <input type="hidden" name="id" value="<?= $edition["id"] ?>">

    </div>
   <div class="form-group">
        <label for="name">Название книги</label>
            <input type="text" class="form-control <?= ($validation->hasError('name')) ? 'is-invalid' : ''; ?>" name="name" value="<?= $edition["name"] ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('name') ?>
            </div>
        </div>
    <div class="form-group">
        <label for="birthday">Изображение</label>
        <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture" value="<?= $edition["picture_url"] ?>" >
        <div class="invalid-feedback">
            <?= $validation->getError('picture') ?>
        </div>
    </div>
    <div class="form-group">
    <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
    </div>
    </form>
    </div>
<?= $this->endSection() ?>

