<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<main class="text-center">
    <div class="jumbotron" style="background:white;">
        <img class="mb-4" width="280" height="280" src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.gZHNn0PNts-OLusiy7PbxwAAAA%26pid%3DApi&f=1" alt="" width="72" height="72"><h1 class="display-4">BookPaste</h1>
        <p class="lead">Это приложение позволит вести учёт взятых книг и контролировать сроки их возврата</p>
        <a class="btn btn-primary btn-lg" href="#" role="button">Войти</a>
    </div>
</main>
<?= $this->endSection() ?>
