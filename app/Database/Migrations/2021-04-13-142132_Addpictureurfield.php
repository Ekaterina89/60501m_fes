<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Addpictureurfield extends Migration
{
	public function up()
	{
		if ($this->db->tableexists('edition'))
        {
            $this->forge->addColumn('edition',array(
                'picture_url' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE)
            ));
        }
	}

	public function down()
	{
		$this->forge->dropColumn('edition', 'picture_url');
	}
}
