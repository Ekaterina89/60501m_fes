<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class LibraryFes extends Migration
{
	public function up()
	{
		// edition
        if (!$this->db->tableexists('edition'))
        {
            // Setup Keys
            $this->forge->addkey('ID', TRUE);

            $this->forge->addfield(array(
                'ID' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                
            ));
            $this->forge->createtable('edition', TRUE);
        }

        //reader
        if (!$this->db->tableexists('reader'))
        {
            // Setup Keys
            $this->forge->addkey('ID', TRUE);

            $this->forge->addfield(array(
                'ID' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE,         'auto_increment' => TRUE),
                'fullname' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                
            ));
            $this->forge->createtable('reader', TRUE);
        }

        //copy
        if (!$this->db->tableexists('copy'))
        {
            // Setup Keys
            $this->forge->addkey('ID', TRUE);

            $this->forge->addfield(array(
                'ID' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE,         'auto_increment' => TRUE),
                'ID_edition' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'wear_factor' => array('type' => 'INT', 'null' => FALSE),
                
            ));
            $this->forge->addForeignKey('ID_edition','edition','ID','RESTRICT','RESRICT');
            $this->forge->createtable('copy', TRUE);
        }

        //delivery
        if (!$this->db->tableexists('delivery'))
        {
            // Setup Keys
            $this->forge->addkey('ID', TRUE);

            $this->forge->addfield(array(
                'ID' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE,         'auto_increment' => TRUE),
                'ID_copy' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'ID_reader' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'take_date' => array('type' => 'DATE', 'null' => FALSE),
                'return_date_plan' => array('type' => 'DATE', 'null' => FALSE),
                'return_date_fact' => array('type' => 'DATE', 'null' => TRUE),
                
            ));
            $this->forge->addForeignKey('ID_copy','copy','ID','RESTRICT','RESRICT');
            $this->forge->addForeignKey('ID_reader','reader','ID','RESTRICT','RESRICT');
            $this->forge->createtable('delivery', TRUE);
        }

	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->droptable('delivery');
         $this->forge->droptable('copy');
         $this->forge->droptable('reader');
        $this->forge->droptable('edition');
	}
}
