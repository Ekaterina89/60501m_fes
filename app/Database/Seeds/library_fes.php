<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class library_fes extends Seeder
{
   public function run()
    {
            //edition           
            $data = [
            'ID' => '1',
            'name'=>'Колобок',
            ];
             $this->db->table('edition')->insert($data);

            $data = [
            'ID' => '2',
            'name'=>'Стрелок',
            ];
             $this->db->table('edition')->insert($data);

$data = [
            'ID' => '3',
            'name'=>'Повелитель мух',
            ];
             $this->db->table('edition')->insert($data);

$data = [
            'ID' => '4',
            'name'=>'Извлечение троих',
            ];
             $this->db->table('edition')->insert($data);

$data = [
            'ID' => '5',
            'name'=>'Колобок',
            ];
             $this->db->table('edition')->insert($data);

$data = [
            'ID' => '6',
            'name'=>'Тёмная башня',
            ];
             $this->db->table('edition')->insert($data);

$data = [
            'ID' => '7',
            'name'=>'Песнь Сюзанны',
            ];
             $this->db->table('edition')->insert($data);

$data = [
            'ID' => '8',
            'name'=>'Извлечение троих',
            ];
             $this->db->table('edition')->insert($data);

$data = [
            'ID' => '9',
            'name'=>'Повелитель мух',
            ];
             $this->db->table('edition')->insert($data);

$data = [
            'ID' => '10',
            'name'=>'Колдун и кристалл',
            ];
             $this->db->table('edition')->insert($data);

//reader          
            $data = [
            'ID' => '1',
            'fullname'=>'Клоков Д.К.',
            ];
             $this->db->table('reader')->insert($data);

            $data = [
            'ID' => '2',
            'fullname'=>'Юрина В.А.',
            ];
             $this->db->table('reader')->insert($data);

$data = [
            'ID' => '3',
            'fullname'=>'Панкова А.С.',
            ];
             $this->db->table('reader')->insert($data);

$data = [
            'ID' => '4',
            'fullname'=>'Ивченко Е.И.',
            ];
             $this->db->table('reader')->insert($data);

$data = [
            'ID' => '5',
            'fullname'=>'Ботина К.А.',
            ];
             $this->db->table('reader')->insert($data);

$data = [
            'ID' => '6',
            'fullname'=>'Гирфанов А.М.',
            ];
             $this->db->table('reader')->insert($data);

$data = [
            'ID' => '7',
            'fullname'=>'Федоренко Е.С.',
            ];
             $this->db->table('reader')->insert($data);

$data = [
            'ID' => '8',
            'fullname'=>'Храмов Д.Д.',
            ];
             $this->db->table('reader')->insert($data);

$data = [
            'ID' => '9',
            'fullname'=>'Власов К.П.',
            ];
             $this->db->table('reader')->insert($data);

$data = [
            'ID' => '10',
            'fullname'=>'Ящук П.В.',
            ];
             $this->db->table('reader')->insert($data);

//copy      
            $data = [
            'ID' => '1',
            'ID_edition'=>'1',
            'wear_factor'=>'10',
            ];
             $this->db->table('copy')->insert($data);

            $data = [
            'ID' => '2',
            'ID_edition'=>'1',
            'wear_factor'=>'8',
            ];
             $this->db->table('copy')->insert($data);

            $data = [
            'ID' => '3',
            'ID_edition'=>'2',
            'wear_factor'=>'9',
            ];
             $this->db->table('copy')->insert($data);

            $data = [
            'ID' => '4',
            'ID_edition'=>'3',
            'wear_factor'=>'7',
            ];
             $this->db->table('copy')->insert($data);

            $data = [
            'ID' => '5',
            'ID_edition'=>'3',
            'wear_factor'=>'6',
            ];
             $this->db->table('copy')->insert($data);

            $data = [
            'ID' => '6',
            'ID_edition'=>'4',
            'wear_factor'=>'7',
            ];
             $this->db->table('copy')->insert($data);

            $data = [
            'ID' => '7',
            'ID_edition'=>'10',
            'wear_factor'=>'3',
            ];
             $this->db->table('copy')->insert($data);

            $data = [
            'ID' => '8',
            'ID_edition'=>'5',
            'wear_factor'=>'5',
            ];
             $this->db->table('copy')->insert($data);

            $data = [
            'ID' => '9',
            'ID_edition'=>'5',
            'wear_factor'=>'7',
            ];
             $this->db->table('copy')->insert($data);

            $data = [
            'ID' => '10',
            'ID_edition'=>'3',
            'wear_factor'=>'3',
            ];
             $this->db->table('copy')->insert($data);

//delivery      
            $data = [
            'ID' => '1',
            'ID_copy'=>'1',
            'ID_reader'=>'1',
            'take_date'=>'2020-05-01',
            'return_date_plan'=>'2020-05-14',
            'return_date_fact'=>'2020-05-14',
            ];
             $this->db->table('delivery')->insert($data);

            $data = [
            'ID' => '2',
            'ID_copy'=>'2',
            'ID_reader'=>'2',
            'take_date'=>'2020-05-06',
            'return_date_plan'=>'2020-05-21',
            'return_date_fact'=>'2020-05-19',
            ];
             $this->db->table('delivery')->insert($data);

            $data = [
            'ID' => '3',
            'ID_copy'=>'3',
            'ID_reader'=>'3',
            'take_date'=>'2020-05-10',
            'return_date_plan'=>'2020-05-25',
            'return_date_fact'=>'2020-05-26',
            ];
             $this->db->table('delivery')->insert($data);

            $data = [
            'ID' => '4',
            'ID_copy'=>'4',
            'ID_reader'=>'4',
            'take_date'=>'2020-05-11',
            'return_date_plan'=>'2020-05-13',
            'return_date_fact'=>'2020-05-15',
            ];
             $this->db->table('delivery')->insert($data);

            $data = [
            'ID' => '5',
            'ID_copy'=>'5',
            'ID_reader'=>'5',
            'take_date'=>'2020-04-01',
            'return_date_plan'=>'2020-05-12',
            'return_date_fact'=>'2020-05-15',
            ];
             $this->db->table('delivery')->insert($data);

            $data = [
            'ID' => '6',
            'ID_copy'=>'6',
            'ID_reader'=>'6',
            'take_date'=>'2020-03-06',
            'return_date_plan'=>'2020-04-06',
            'return_date_fact'=>'2020-04-08',
            ];
             $this->db->table('delivery')->insert($data);

            $data = [
            'ID' => '7',
            'ID_copy'=>'7',
            'ID_reader'=>'7',
            'take_date'=>'2020-05-10',
            'return_date_plan'=>'2020-05-12',
            'return_date_fact'=>'2020-05-05',
            ];
             $this->db->table('delivery')->insert($data);

            $data = [
            'ID' => '8',
            'ID_copy'=>'8',
            'ID_reader'=>'8',
            'take_date'=>'2020-05-10',
            'return_date_plan'=>'2020-05-25',
            'return_date_fact'=>'2020-05-26',
            ];
             $this->db->table('delivery')->insert($data);

            $data = [
            'ID' => '9',
            'ID_copy'=>'9',
            'ID_reader'=>'9',
            'take_date'=>'2020-05-11',
            'return_date_plan'=>'2020-05-13',
            'return_date_fact'=>'2020-05-10',
            ];
             $this->db->table('delivery')->insert($data);

            $data = [
            'ID' => '10',
            'ID_copy'=>'10',
            'ID_reader'=>'10',
            'take_date'=>'2020-05-01',
            'return_date_plan'=>'2020-05-12',
            'return_date_fact'=>'2020-05-12',
            ];
             $this->db->table('delivery')->insert($data);

    }
}
