<?php namespace App\Services;

use Google_Client;

class GoogleClient
{
    private $google_client;
    public function __construct()
    {
        $this->google_client = new Google_Client();
        $this->google_client->setClientId('885177589688-7c59ub2u4fqc35ugk9vlvsg43op8067l.apps.googleusercontent.com');
        $this->google_client->setClientSecret('2YHicioev9ZeV8SyRO-5SQhb');
        $this->google_client->setRedirectUri(base_url().'/auth/google_login');
        $this->google_client->addScope('email');
        $this->google_client->addScope('profile');
    }
    public function getGoogleClient()
    {
        return $this->google_client;
    }

}
