<?php namespace App\Models;
use CodeIgniter\Model;
class DeliveryModel extends Model
{
    protected $table = 'delivery'; //таблица, связанная с моделью
    protected $allowedFields = ['id', 'ID_copy', 'ID_reader', 'take_date', 'return_date_plan','return_date_fact'];    
    public function getDelivery($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        else return $this->select('d.id, d.ID_copy, e.name, d.ID_reader, r.fullname, d.take_date, d.return_date_plan, d.return_date_fact')->distinct()->from('delivery d')->join('copy c', 'd.ID_copy=c.id')->join('edition e','c.ID_edition=e.id')->join('reader r','d.ID_reader=r.ID')->where('e.id',$id)->findAll();
    }
    public function getOneDelivery($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        else return $this->select('d.id, d.ID_copy, d.ID_reader, r.fullname, d.take_date, d.return_date_plan, d.return_date_fact')->distinct()->from('delivery d')->join('copy c', 'd.ID_copy=c.id')->join('edition e','c.ID_edition=e.id')->join('reader r','d.ID_reader=r.ID')->where('d.id',$id)->first();
    }

public function getDeliveryWithEdition($id = null, $search = '')
{
    $builder = $this->select('d.id, e.picture_url, d.ID_copy,e.name, c.wear_factor, d.ID_reader, r.fullname, d.take_date, d.return_date_plan, d.return_date_fact')->distinct()->from('delivery d')->join('copy c', 'd.ID_copy=c.id')->join('edition e','c.ID_edition=e.id')->join('reader r','d.ID_reader=r.ID')->like('e.name', $search,'both', null, true)->orlike('r.fullname',$search,'both',null,true);
    if (!is_null($id))
    {
        return $builder->where(['delivery.id' => $id])->first();
    }
    return $builder;
}

}
