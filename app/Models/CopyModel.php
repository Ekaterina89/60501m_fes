<?php namespace App\Models;
use CodeIgniter\Model;
class CopyModel extends Model
{
    protected $table = 'copy'; //таблица, связанная с моделью
    protected $allowedFields = ['id', 'ID_edition', 'wear_factor'];    
    public function getCopy($id = null)
    {
        if (!isset($id)) {
            return $this->select('c.id, e.name')->distinct()->from('copy c')->join('edition e', 'c.ID_edition=e.id')->findAll();
        }
        return $this->select('c.id, e.name')->distinct()->from('copy c')->join('edition e', 'c.ID_edition=e.id')->where('c.id',$id)->findAll();
    }
}
