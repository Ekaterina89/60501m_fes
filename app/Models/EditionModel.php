<?php namespace App\Models;
use CodeIgniter\Model;
class EditionModel extends Model
{
    protected $table = 'edition'; //таблица, связанная с моделью
    protected $allowedFields = ['id', 'name','picture_url'];    
    public function getEdition($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}
