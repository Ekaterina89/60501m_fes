<?php namespace App\Models;
use CodeIgniter\Model;
class ReaderModel extends Model
{
    protected $table = 'reader'; //таблица, связанная с моделью
    protected $allowedFields = ['id', 'fullname'];    
    public function getReader($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where('id',$id)->findAll();
    }
}
